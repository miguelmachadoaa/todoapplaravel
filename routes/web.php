<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TareasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


    Route::get('list', 'App\Http\Controllers\TareasController@list')->name('list');

    Route::get('completadas', 'App\Http\Controllers\TareasController@completadas')->name('completadas');

    Route::get('pendientes', 'App\Http\Controllers\TareasController@pendientes')->name('pendientes');

    Route::get('gettarea/{id}', 'App\Http\Controllers\TareasController@gettarea')->name('gettarea');
  #  Route::post('store', 'App\Http\Controllers\TareasController@store')->name('store');

    Route::post('store', 'App\Http\Controllers\TareasController@store')->name('store.post');
    Route::options('store', 'App\Http\Controllers\TareasController@store')->name('store.options');

  #  Route::options('store', function () {        return true;    });

    Route::post('update/{id}', 'App\Http\Controllers\TareasController@update')->name('update.post');
    Route::options('update/{id}', 'App\Http\Controllers\TareasController@update')->name('update.options');


    Route::post('update/{id}', 'App\Http\Controllers\TareasController@update')->name('update');

    Route::get('delete/{id}', 'App\Http\Controllers\TareasController@destroy')->name('delete');

    Route::get('complete/{id}', 'App\Http\Controllers\TareasController@complete')->name('complete');

    Route::get('buscar/{buscar}', 'App\Http\Controllers\TareasController@buscar')->name('buscar');