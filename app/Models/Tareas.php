<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Tareas extends Model
{
    use SoftDeletes;

    public $table = 'tareas';        
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'nota',
        'completed'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
}
